class InvoiceMailer < ActionMailer::Base
  default from: "mikk.romulus@gmail.com"
  layout "email"

  def paid_email(invoice)
    @invoice = invoice
    mail(to: @invoice.email, subject: 'Thank you for the purchase!')
  end
end
