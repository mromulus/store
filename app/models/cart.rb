class Cart < ActiveRecord::Base
  has_many :cart_lines
  has_many :products, through: :cart_lines
  has_one :invoice

  ##
  # Add a product to this cart
  #
  # If product already exists in the cart, increment its count
  def add_item(product)
    return false if locked? || product.out_of_stock?
    cart_lines.where(product: product).first_or_create.increment! :count
    product.decrement! :in_stock
  end

  ##
  # Removes a product from this cart
  def delete_item(product)
    return false if locked?
    line_for(product).destroy
  end

  ##
  # Total price for cart
  #
  # Optional parameter 'at' to specify the time, used in invoicing
  def amount(at = Time.now)
    cart_lines.collect do |cl|
      cl.amount(at)
    end.sum
  end

  ##
  # Human readable price for this cart
  def human_amount(at = Time.now)
    "#{amount(at)}€"
  end

  def has_products?
    products.present?
  end

  def line_for(product)
    cart_lines.where(product: product).first
  end

  ##
  # Set this cart as paid, generates invoice
  def process_as_paid!(email)
    return if locked?
    invoice = Invoice.for(self, email)
    save
  end

  private

  def locked?
    invoice.present?
  end
end
