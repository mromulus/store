class Category < ActiveRecord::Base
  validates :name, length: { minimum: 4 }
  validate :parent_in_available_parents

  has_many :products

  belongs_to :parent, class_name: "Category"
  has_many :children, class_name: "Category", foreign_key: "parent_id"

  scope :first_level, -> { where(parent_id: nil) }
  scope :third_level_categories, -> { joins(parent: :parent) }

  ##
  # Finds valid parent categories for a single category instance
  #
  # Excludes self and third level categories
  def available_parents
    s = self.class.all
    if self.persisted?
      s = s.where(["id != ?", self.id])
    end

    if self.class.third_level_categories.present?
      s = s.where(["id not in (?)", self.class.third_level_categories.collect(&:id)])
    end
    s
  end

  private

  def parent_in_available_parents
    if parent.present? && !available_parents.include?(parent)
      errors.add(:parent_id, "is not allowed")
    end
  end
end
