class Product < ActiveRecord::Base
  belongs_to :category, counter_cache: true
  has_many :product_pricings, dependent: :destroy
  has_many :discounts, dependent: :destroy

  accepts_nested_attributes_for :product_pricings, reject_if: lambda {|attributes|
    attributes['price'].blank?
  }

  accepts_nested_attributes_for :discounts, reject_if: lambda {|attributes|
    attributes['amount'].blank?
  }

  validates :name, length: { minimum: 4, maximum: 200 }
  validates :in_stock, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :category, presence: true

  def human_price_with_discount(d, p)
    "#{price_with_discount(d, p)}€"
  end

  ##
  # Calculate the price given the discount and pricing
  #
  # Ensures no spillover to different pricing period
  def price_with_discount(d, p)
    price_at([d.takes_effect_on, p.takes_effect_on].max)
  end

  ##
  # Calculate the price at any given time, accounting for discount
  def price_at(time)
    p = pricing_at(time).price
    if discount_at(time)
      p - discount_at(time).calculate_discount(p)
    else
      p
    end
  end

  def price
    price_at Time.now
  end

  def human_price
    "#{price}€"
  end

  def out_of_stock?
    in_stock <= 0
  end

  def discounted?
    discount_at(Time.now).present?
  end

  def current_pricing
    pricing_at(Time.now)
  end

  def active_discounts_for(pricing)
    discounts.where([
      "takes_effect_on between :start and :end or
      ends_on between :start and :end or
      takes_effect_on < :start and ends_on > :end",
      start: pricing.takes_effect_on,
      end: next_pricing(pricing).takes_effect_on
    ])
  end

  def next_pricing(pricing)
    product_pricings.where(["takes_effect_on > ?", pricing.takes_effect_on]).order("takes_effect_on asc").first || NullPricing.new
  end

  private

  def discount_at(time)
    discounts.where(["? between takes_effect_on and ends_on", time]).first
  end

  def pricing_at(time)
    product_pricings.order("takes_effect_on desc").where(["takes_effect_on <= ?", time]).first
  end
end