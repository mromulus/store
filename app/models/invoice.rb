class Invoice < ActiveRecord::Base
  belongs_to :cart

  delegate :human_amount, :cart_lines, to: :cart

  before_create :generate_hash

  def self.for(cart, email)
    cart.invoice = self.create({
      email: email
    })
    InvoiceMailer.paid_email(cart.invoice).deliver
    cart.invoice
  end

  def to_param
    token
  end

  private

  def generate_hash
    self.token = Digest::SHA1.hexdigest "f837d27ea5d8e7c783043fb52a-#{self.id}-9179849b6bf28-#{Time.now.to_f}-0b7a9bf8050aeef957"
  end
end