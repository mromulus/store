class CartLine < ActiveRecord::Base
  belongs_to :cart
  belongs_to :product

  after_destroy :return_product_in_stock

  def amount(at = Time.now)
    count * product.price_at(at)
  end

  def human_amount(at = Time.now)
    "#{amount}€"
  end

  private

  def return_product_in_stock
    product.increment! :in_stock, count
  end
end
