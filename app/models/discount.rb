class Discount < ActiveRecord::Base
  belongs_to :product

  validates :takes_effect_on, { presence: true }
  validates :ends_on, { presence: true }
  validates :amount, { presence: true }

  validate :avoid_overlapping_discounts
  validate :not_in_past
  validate :not_negative_price

  before_validation :fix_timestamp_ordering

  ##
  # Calculate the absolute discount given the price
  def calculate_discount(price)
    if percentage_based?
      price * amount / 100.0
    else
      amount
    end
  end

  private

  def overlapping_discounts
    product.discounts.where([
      "ends_on > :start and takes_effect_on < :start or takes_effect_on < :end and ends_on > :end",
      start: self.takes_effect_on,
      end: self.ends_on
    ])
  end

  def avoid_overlapping_discounts
    unless overlapping_discounts.empty?
      errors.add :takes_effect_on, "Overlapping discounts are not allowed"
    end
  end

  def not_in_past
    if takes_effect_on && takes_effect_on.past?
      errors.add(:takes_effect_on, "can't be in past")
    end
  end

  def not_negative_price
    p = product.price_at(takes_effect_on)
    if (p - calculate_discount(p)) <= 0
      errors.add(:amount, "can't make the price negative")
    end
  end

  # if user chooses earlier ending time than beginning, fix the ordering
  def fix_timestamp_ordering
    if takes_effect_on && ends_on && ends_on < takes_effect_on
      self.takes_effect_on, self.ends_on = ends_on, takes_effect_on
    end
  end
end
