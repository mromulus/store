class ProductPricing < ActiveRecord::Base
  belongs_to :product

  validates :takes_effect_on, { presence: true }
  validates :price, presence: true, numericality: { greater_than_or_equal_to: 0.01 }

  validate :not_in_past

  def human_price
    "#{price}€"
  end

  private

  def not_in_past
    if takes_effect_on && takes_effect_on < Time.now-30.seconds
      errors.add(:takes_effect_on, "can't be in past")
    end
  end
end