# from https://github.com/plataformatec/simple_form/issues/886
# since simple_form does not play nice with bootstrap 3 yet

class StackedRadioButtonsInput < SimpleForm::Inputs::CollectionRadioButtonsInput

  # Remove all classes from the input
  def initialize(*)
    super
    @input_html_options[:class] = []
  end

  # Wrap each item in a div
  def apply_nested_boolean_collection_options!(options)
    options[:item_wrapper_tag] = :div
  end

  # Add labels back
  def build_nested_boolean_style_item_tag(collection_builder)
    "<label>".html_safe + collection_builder.radio_button + collection_builder.text + "</label>".html_safe
  end

  # Act as a normal radio buttons collection
  def input_type
    :radio_buttons
  end

end