class CartController < ApplicationController
  before_filter :store_email

  def show
    @email = session[:email]
  end

  def charge
    if session[:email].present?
      @email = session[:email]
      if params[:stripeToken]
        amount = (cart.amount*100).to_i

        customer = Stripe::Customer.create(
          :email => @email,
          :card  => params[:stripeToken]
        )

        charge = Stripe::Charge.create(
          :customer    => customer.id,
          :amount      => amount,
          :description => 'SuperStore test payment',
          :currency    => 'eur'
        )

        if charge["paid"]
          cart.process_as_paid! @email
          session.delete :cart_id
        end
      else
        render :show
      end
    else
      render :show
    end
  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to checkout_path
  end

  private

  def store_email
    if params[:email]
      session[:email] = params[:email]
    end
  end
end