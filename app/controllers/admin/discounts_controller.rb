class Admin::DiscountsController < AdminController
  def destroy
    discount = Discount.find(params[:id])
    if discount && discount.takes_effect_on.future?
      discount.destroy
    else
      flash[:error] = "Can't remove discounts in the past"
    end
    redirect_to :back
  end
end