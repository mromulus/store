class Admin::ProductsController < AdminController
  before_filter :get_product, only: [:edit, :show, :update, :destroy]
  before_filter :get_category, only: [:index]

  def index
    @products = Product.all
    if @category
      @products = @products.where(category: @category)
    end
  end

  def new
    @product = Product.new
  end

  def edit
  end

  def create
    @product = Product.new(product_params)
    @product.product_pricings.first_or_initialize.takes_effect_on = Time.now
    if @product.save
      redirect_to admin_products_path, notice: "Product #{@product.name} created"
    else
      render action: "new"
    end
  end

  def update
    if @product.update_attributes(product_params)
      redirect_to product_redirection_path, notice: "Product #{@product.name} updated"
    else
      render action: "edit"
    end
  end

  private

  def product_redirection_path
    if @product.category
      [:admin, @product.category, :products]
    else
      admin_products_path
    end
  end

  def product_params
    params.require(:product).permit(:name, :description, :in_stock, :category_id,
      product_pricings_attributes: [:price, :takes_effect_on],
      discounts_attributes: [:takes_effect_on, :ends_on, :amount, :percentage_based]
    )
  end

  def get_product
    @product = Product.find(params[:id], include: [:discounts, :product_pricings])
    redirect_to admin_products_path unless @product
  end

  def get_category
    if params[:category_id]
      @category = Category.find(params[:category_id])
    end
  end
end