class Admin::ProductPricingsController < AdminController
  def destroy
    productpricing = ProductPricing.find(params[:id])
    if productpricing && productpricing.takes_effect_on.future?
      productpricing.destroy
    else
      flash[:error] = "Can't remove price in the past"
    end
    redirect_to :back
  end
end