class Admin::CategoriesController < AdminController
  before_filter :get_category, only: [:edit, :show, :update, :destroy]

  def index
    @categories = Category.all
  end

  def new
    @category = Category.new
  end

  def edit
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to admin_categories_path, notice: "category #{@category.name} created"
    else
      render action: "new"
    end
  end

  def update
    if @category.update_attributes(category_params)
      redirect_to admin_categories_path, notice: "category #{@category.name} updated"
    else
      render action: "edit"
    end
  end

  private

  def category_params
    params.require(:category).permit(:name, :parent_id)
  end

  def get_category
    @category = Category.find(params[:id])
    redirect_to admin_categories_path unless @category
  end
end