class InvoicesController < ApplicationController
  layout "blank"

  def show
    @invoice = Invoice.where(token: params[:id]).first
    unless @invoice
      return render status: 404
    end
  end
end