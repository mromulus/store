class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :cart

  protected

  def cart
    if session[:cart_id]
      Cart.find session[:cart_id], include: [cart_lines: :product]
    else
      c = Cart.create
      session[:cart_id] = c.id
      c
    end
  end
end
