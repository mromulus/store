class ProductsController < ApplicationController
  before_filter :get_category
  before_filter :get_product, except: [:index]

  def index
    @products = if @category
      @category.products
    else
      Product.all
    end
  end

  def show
  end

  def add_to_cart
    unless cart.add_item @product
      flash[:notice] = "#{product.name} is out of stock, you cannot add any more!"
    end
    redirect_to @product
  end

  def delete_from_cart
    cart.delete_item @product
    redirect_to :back
  end

  private

  def get_category
    if params[:category_id]
      @category = Category.find params[:category_id]
    end
  end

  def get_product
    @product = Product.find(params[:id])
  end
end