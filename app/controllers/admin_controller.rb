class AdminController < ApplicationController
  before_filter :require_authentication

  layout "admin"

  private

  def require_authentication
    # simple http basic auth for now
    authenticate_or_request_with_http_basic do |username, password|
      password == "letmein"
    end
  end
end