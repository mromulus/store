class CreateProductPricings < ActiveRecord::Migration
  def change
    create_table :product_pricings do |t|
      t.references :product, index: true
      t.decimal :price, null: false, precision: 6, scale: 2
      t.datetime :takes_effect_on, null: false

      t.timestamps
    end
  end
end
