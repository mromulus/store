class CreateDiscounts < ActiveRecord::Migration
  def change
    create_table :discounts do |t|
      t.references :product, index: true
      t.datetime :takes_effect_on, null: false
      t.integer :duration, null: false
      t.integer :amount, null: false
      t.boolean :percentage_based, null: false, default: true

      t.timestamps
    end
  end
end
