class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.integer :in_stock, :default => 0

      t.timestamps
    end
  end
end
