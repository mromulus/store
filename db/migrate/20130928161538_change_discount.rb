class ChangeDiscount < ActiveRecord::Migration
  def change
    remove_column :discounts, :duration, :integer
    add_column :discounts, :ends_on, :datetime

    change_column :discounts, :amount, :decimal, scale: 2, precision: 10
  end
end
