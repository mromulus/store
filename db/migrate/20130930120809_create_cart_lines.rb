class CreateCartLines < ActiveRecord::Migration
  def change
    create_table :cart_lines do |t|
      t.references :cart, index: true
      t.references :product, index: true
      t.integer :count, default: 0

      t.timestamps
    end
  end
end
