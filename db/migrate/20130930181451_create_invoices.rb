class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.references :cart, index: true
      t.string :email
      t.string :token, null: false

      t.timestamps
    end
  end
end
