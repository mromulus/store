# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130930181451) do

  create_table "cart_lines", force: true do |t|
    t.integer  "cart_id"
    t.integer  "product_id"
    t.integer  "count",      default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cart_lines", ["cart_id"], name: "index_cart_lines_on_cart_id", using: :btree
  add_index "cart_lines", ["product_id"], name: "index_cart_lines_on_product_id", using: :btree

  create_table "carts", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.integer  "products_count"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "parent_id"
  end

  create_table "discounts", force: true do |t|
    t.integer  "product_id"
    t.datetime "takes_effect_on",                                          null: false
    t.decimal  "amount",           precision: 10, scale: 2,                null: false
    t.boolean  "percentage_based",                          default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "ends_on"
  end

  add_index "discounts", ["product_id"], name: "index_discounts_on_product_id", using: :btree

  create_table "invoices", force: true do |t|
    t.integer  "cart_id"
    t.string   "email"
    t.string   "token",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invoices", ["cart_id"], name: "index_invoices_on_cart_id", using: :btree

  create_table "product_pricings", force: true do |t|
    t.integer  "product_id"
    t.decimal  "price",           precision: 6, scale: 2, null: false
    t.datetime "takes_effect_on",                         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_pricings", ["product_id"], name: "index_product_pricings_on_product_id", using: :btree

  create_table "products", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "in_stock",    default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_id"
  end

  add_index "products", ["category_id"], name: "index_products_on_category_id", using: :btree

end
