# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Store::Application.config.secret_key_base = 'e6edb118e0c59a49f745a2e9090f302b85bec2498f020433a0768f4d103ed7a658a5e53804aeba552360bf93b01ec61d63f565f536449d4f3fc6d96e5b1e5196'
