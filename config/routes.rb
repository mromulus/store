Store::Application.routes.draw do
  root 'products#index'

  controller :cart do
    get "/checkout",  to: "cart#show"
    post "/checkout", to: "cart#charge"
  end

  resources :invoices, only: [:show]

  resources :products, only: [:index, :show] do
    get '/c/:category_id', on: :collection, action: :index, as: :category
    post   'cart', on: :member, action: :add_to_cart, as: :add_to_cart
    delete 'cart', on: :member, action: :delete_from_cart, as: :delete_from_cart
  end

  namespace :admin do
    get '/', to: redirect('/admin/categories')

    resources :products
    resources :categories do
      resources :products, only: [:index]
    end

    resources :discounts, only: [:destroy]
    resources :product_pricings, only: [:destroy]
  end
end
