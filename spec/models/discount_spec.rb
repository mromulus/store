require 'spec_helper'

describe Discount do
  let(:p) { FactoryGirl.create(:product_with_price) }
  let(:d) { FactoryGirl.create(:percent_discount, product: p) }
  describe "calculate_discount" do

    it "should work with percent type discount" do
      expect(d.calculate_discount(p.current_pricing.price)).to eq(0.1)
    end

    it "should work with flat discount" do
      d = FactoryGirl.create(:flat_discount, product: p)
      expect(d.calculate_discount(p.current_pricing.price).to_f).to eq(0.2)
    end
  end

  it "should not allow discounts with overlapping intervals" do
    d # let's invoke the factories
    expect(FactoryGirl.build(:percent_discount, product: p, takes_effect_on: Time.now+4.hours).valid?).to be_false
  end

  it "should switch around end and start time if mixed up" do
    now = Time.now
    d = FactoryGirl.create(:percent_discount, product: p, ends_on: now+1.hour, takes_effect_on: now+2.hours)

    d.valid?

    expect(d.takes_effect_on).to eq(now+1.hour)
    expect(d.ends_on).to eq(now+2.hours)
  end

  it "should not allow negative price (percent)" do
    d.amount = 120
    expect(d.valid?).to be_false
  end

  it "should not allow negative price (flat)" do
    d = FactoryGirl.create(:flat_discount, product: p)
    d.amount = 1.0
    expect(d.valid?).to be_false
  end
end
