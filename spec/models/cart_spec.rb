require 'spec_helper'

describe Cart do
  let(:c) { FactoryGirl.create :cart }
  describe "add_item" do
    let(:p) { FactoryGirl.create :product }

    it "should create a cart_line" do
      c.add_item p

      expect(c.products).to include(p)
      expect(c.cart_lines.first.count).to eq(1)
    end

    it "should increment count product already present" do
      FactoryGirl.create(:cart_line, product: p, cart: c, count: 4)
      c.add_item p

      expect(c.cart_lines.first.count).to eq(5)
    end

    it "should not work if cart is locked (with invoice)" do
      c.add_item p

      c.invoice = FactoryGirl.create(:invoice, cart: c)
      expect(c.add_item(p)).to be_false
      expect(c.line_for(p).count).to be(1)
    end

    it "should not allow adding to cart when product is out of stock" do
      p.in_stock = 1
      # first time
      expect(c.add_item(p)).to_not be_false
      # out of stock now
      expect(c.add_item(p)).to be_false

      expect(c.line_for(p).count).to be(1)
    end
  end

  describe "amount" do
    it "should give a sum amount for all lines in cart" do
      2.times { FactoryGirl.create(:cart_line, cart: c) }
      CartLine.any_instance.stub(:amount).and_return(2)

      expect(c.amount).to eq(4)
    end
  end

  describe "has_products?" do
    it "should tell if there are products present in cart" do
      expect(c.has_products?).to be_false

      2.times { c.add_item FactoryGirl.create(:product) }
      c.reload # for some reason we need to reload the cart model
      expect(c.has_products?).to be_true
    end
  end

  it "process_as_paid!" do
    email = "test@example.com"
    product = FactoryGirl.create(:product, in_stock: 10)
    Invoice.should_receive(:for).with(c, email)
    2.times { c.add_item product }
    expect(product.in_stock).to eq(8)

    c.process_as_paid! email
    product.reload

  end
end
