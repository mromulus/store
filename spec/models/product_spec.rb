require 'spec_helper'

describe Product do
  let(:p) { FactoryGirl.create(:product) }

  describe "next_pricing" do
    it "should return following effective pricing, when available" do
      p = FactoryGirl.create(:product_with_price)

      nextone = FactoryGirl.create(:product_pricing, takes_effect_on: Time.now + 1.month, product: p)

      expect(p.next_pricing(p.current_pricing)).to eq(nextone)
    end

    it "should give null object with far future start time when none found" do
      p = FactoryGirl.create(:product_with_price)

      expect(p.next_pricing(p.current_pricing).takes_effect_on).to be > Time.now+99.years
    end
  end

  describe "discounts" do
    let(:p) { FactoryGirl.create(:product_with_price) }
    let(:d) { FactoryGirl.create(:percent_discount, product: p) }

    it "should show up in active discounts" do
      expect(p.active_discounts_for(p.current_pricing)).to include(d)
    end

    it "should show up in active discounts for pricing entirely contained in discount period" do
      pricing = FactoryGirl.create(:product_pricing, product: p, takes_effect_on: Time.now+10.days)
      FactoryGirl.create(:product_pricing, product: p, takes_effect_on: Time.now+11.days)
      discount = FactoryGirl.create(:percent_discount, takes_effect_on: Time.now+8.days, ends_on: Time.now+12.days, product: p)

      expect(p.active_discounts_for(pricing)).to eq([discount])
    end

    it "should not show discounts for another pricing" do
      FactoryGirl.create(:product_pricing, product: p, takes_effect_on: Time.now+6.days)
      FactoryGirl.create(:percent_discount, product: p, takes_effect_on: Time.now+7.days)

      expect(p.active_discounts_for(p.current_pricing)).to be_empty
    end
  end
end