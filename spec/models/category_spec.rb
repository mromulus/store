require 'spec_helper'

describe Category do
  describe "available_parents" do
    let(:c) { FactoryGirl.create(:category) }

    it "should list all categories except self" do
      3.times { FactoryGirl.create(:category) }

      expect(c.available_parents.size).to eq(3)
      expect(c.available_parents).to_not include(c)
    end

    it "should not list 3rd level categories as available" do
      second_level = FactoryGirl.create(:category, parent: c, name: "child1")
      third_level = FactoryGirl.create(:category, parent: second_level, name: "child2")

      3.times { FactoryGirl.create(:category) }

      expect(c.available_parents.size).to eq(4)

      expect(c.available_parents).to include(second_level)
      expect(c.available_parents).to_not include(third_level)
    end

    it "should list all categories when called on a unpersisted category with no 3rd level categories" do
      c = FactoryGirl.build(:category)
      3.times { FactoryGirl.create(:category) }

      expect(c.available_parents).to eq(Category.all)
    end

    it "should add an error if attempting to set illegal parent" do
      second_level = FactoryGirl.create(:category, parent: c, name: "child1")
      third_level = FactoryGirl.create(:category, parent: second_level, name: "child2")

      c.parent = third_level

      expect(c.valid?).to be(false)
    end
  end
end
