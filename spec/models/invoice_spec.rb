require 'spec_helper'

describe Invoice do
  describe "for" do
    let(:p) { FactoryGirl.create(:product_with_price) }
    let(:c) { FactoryGirl.create(:cart) }
    it "should generate a new invoice based on cart contents" do
      c.add_item p
      email = "test@example.com"

      i = Invoice.for(c, email)

      expect(i.email).to eq(email)
      expect(i.hash).to_not be_false
      c.reload
      expect(c.invoice).to eq(i)
    end
  end
end