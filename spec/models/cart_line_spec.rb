require 'spec_helper'

describe CartLine do
  let(:p) { FactoryGirl.create :product_with_price }
  let(:cl) { FactoryGirl.create :cart_line, product: p }

  it "should sum products current price as amount" do
    cl.count = 2

    expect(cl.amount).to eq(2.0)
  end
end
