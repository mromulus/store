# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :cart_line do
    cart nil
    product nil
    count 1
  end
end
