FactoryGirl.define do
  factory :product_pricing do
    price 1.00

    takes_effect_on Time.now
  end

  factory :category do
    name 'Group #1'
  end

  factory :product do
    name 'Produkt #1'
    in_stock 2
    description "It's a german thing"

    category

    factory :product_with_price do
      after(:create) do |p|
        FactoryGirl.create(:product_pricing, product: p)
      end
    end
  end


  factory :discount do
    takes_effect_on Time.now+30.minutes
    ends_on { |d| d.takes_effect_on + 6.hours }


    factory :percent_discount do
      amount 10
      percentage_based true
    end

    factory :flat_discount do
      amount 0.2
      percentage_based false
    end
  end
end